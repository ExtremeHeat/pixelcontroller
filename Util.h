#ifndef UTIL_H
#define UTIL_H

#include <string.h>

unsigned int randr(unsigned int min, unsigned int max) {
	double scaled = (double)rand() / RAND_MAX;

	return (max - min + 1) * scaled + min;
}

// My bootleg string split!
int splitString(char delimiter, String &what, int &pos, String &out) {
	auto len = what.length();
	if (pos == len) return 0;
	for (int i = pos + 1; i < len; i++) {
		auto c = what[i];
		if (c == delimiter) {
			auto s = what.substring(pos == 0 ? pos : pos + 1, i);
			pos = i;
			out = s;
			return i;
		}
	}
	out = what.substring(pos == 0 ? pos : pos + 1, (int)len);
	pos = len;
	return len;
}

#endif