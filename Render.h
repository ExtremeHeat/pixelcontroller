#ifndef RENDER_H
#define RENDER_H
#include <FastLED.h>
#include "Util.h"

#define NUM_LEDS 8 * 32
#define DATA_PIN 6

CRGB leds[NUM_LEDS];

int pixelMapToNP(int x, int y) {
	int pixnum = x * 8;
	if (x % 2) {
		pixnum += 7 - (y % 8);
	} else {
		pixnum += y % 8;
	}

	return pixnum;
}

void draw(int offset, long color) {
	u8 green = (color & 0xFF000000) >> 24;
	u8 red = (color & 0x00FF0000) >> 16;
	u8 blue = (color & 0x0000FF00) >> 8;

	if (color == 0xf0)
	{
		red = randr(0, 255);
		green = randr(0, 255);
		blue = randr(0, 255);
	}

	//printf("FakeDraw(%d,%d) %d RGB(%d, %d, %d)\n", x, y, offset, red, green, blue);

	if (offset >= NUM_LEDS) {
		return;
	}

	printf("offset %d\n", offset);
	leds[offset] = CRGB(red, /*randr(0x20, 0xEE)*/ green, blue);
	return;
}

void draw(int x, int y, long color) {
	int offset = pixelMapToNP(x, y);

	//printf("FakeDraw(%d,%d) %d RGB(%d, %d, %d)\n", x, y, offset, red, green, blue);

	if (offset >= NUM_LEDS) {
		return;
	}

	//printf("offset %d\n", offset);
	//leds[offset] = CRGB(red, /*randr(0x20, 0xEE)*/green, blue);
	draw(offset, color);
	return;
}

#define RB pgm_read_byte

char drawChar(char xoff, char yoff, char c, long rgb) {
	//auto pm = C[c];
	const XY *pm = 0;
	int len = 0;
	getChar(c, pm, len);
	if (!len)
	{
		return 1; // dont have pixel map for char, spaces
	}
	//printf("pm.x, pm.y %d %d\n", char0[0].x, char0[0].y);
	int cwidth = 0;
	for (int i = 0; i < len; i++)
	{
		auto pix = &pm[i];
		cwidth = MAX(cwidth, RB(&pix->x) + 1);
		auto x = RB(&pix->x) + xoff;
		auto y = RB(&pix->y) + yoff;
		if (x >= PIXEL_COLUMNS || y >= PIXEL_ROWS || x < 0 || y < 0)
			continue;
		draw(x, y, rgb);
		// table[y][x] = style || 'background-color: green;';
	}
	return cwidth;
}

int write(String text, long rgba) {
	auto x = 0;
	auto lwlen = 0;
	for (auto c : text) {
		lwlen = drawChar(x, 0, c, rgba);
		x += lwlen + 1;
	}

    return x;
}

void write(String text, long rgba, int x, int y = 0) {
	auto lwlen = 0;
	for (auto c : text) {
		lwlen = drawChar(x, y, c, rgba);
		x += lwlen + 1;
	}
}

void write(int num, long rgba) {
    char str[4];
	sprintf(str, "%d", num);

	//write("HELLO", 0xff000000);
	write(str, 0x00ff0000);
}

/*void write(char *data, int len, long rgba) {
	auto x = 0;
	auto lwlen = 0;
	for (int i = 0; i < len; i++) {
	auto c = data[i];
	lwlen = drawChar(x, 0, c, rgba);
	x += lwlen + 1;
	}

}*/

void render()
{
	FastLED.show();
}

void flush() {
	for (int i = 0; i < NUM_LEDS; i++)
	{
		leds[i] = 0;
	}
}

#endif
