#include <FastLED.h>
#include <string.h>
#include "PixelFonts.h"
#include "Render.h"
#include "Values.h"
#include "Util.h"
#include "PixelSequences.h"
#include "SerialCom.h"

void importantmessage() {
	FastLED.setBrightness(20);
}

void setup() {
	// put your setup code here, to run once:
	FastLED.addLeds<WS2812, DATA_PIN>(leds, NUM_LEDS);
	FastLED.setBrightness(1);
    Serial.begin(9600);
}

void (*executable)(void) = nullptr;

void readSerial() {
	String incoming;
	int readInt = 0;
	if (Serial.available() > 0) {
		Serial.println("in!");

		if (sState == SerialReadingState::AwaitingTimesync) {
      		readInt = Serial.parseInt();
			sProcessTimestamp(readInt);
		} else {
      		incoming = Serial.readString();
			sProcessMessage(incoming);
		}

    	// if (expectedType == sMessage) {
      	// 	incoming = Serial.readString();
      	// 	auto len = incoming.length();

       	// 	if (len > 3) {
		// 		sProcessMessage(incoming);
        		
				      
		// 	}
			
		// 	Serial.println("[" + incoming + "]");
		// } else if (expectedType == 1) {
		// 	//      flush(); write("I", 0xf0); render();

      	// 	readInt = Serial.parseInt();
      	// 	flush();      write(readInt, 0); render();
      	// 	Serial.println("Read int, reverting to Str:");
      	// 	Serial.println(readInt);
      	// 	expectedType=0;
    	// }
	}
}

int WAIT_TIME = 100;

// void run_amber() {
// 	String h = "AMBER ALERT";

// 	FastLED.setBrightness(20);

// 	writeFlashing(h, 0xFF00);

// 	FastLED.setBrightness(2);

// 	String k = "joelle turner";

// 	writeScrollable(k, 0xFF00, 400);

// 	String l = "15, Missing: 07/05/2019. Missing From BRONX, NY";

// 	writeFlashing(l, 0xFF00);
// }

void loop() {
	loops++;

// run_amber();
	// Serial.println(loops);

	// flush();
	// write("Hello", 0xf0);
	// render();


	readSerial();

	// delay(1000);
	// return;

	run_disco();

	if (gState == State::Nothing) {
		delay(WAIT_TIME);
	} else if (gState == State::DisplayingText) {
		qrDisplayMessage();
		if (sState == RecievingBufferedACK) {
			sSendACK();
		}
	} else {
		delay(WAIT_TIME);
	}


//  render();
//flush();
//render();
//   delay(100);
//    String text = "Due to recent severe heat, traffic signals without power may be present in various parts of NYC";
//  renderLoopText(text, text.length(), 0xf0);
//  writeFlashing(text, 0xFFFF00);
//  String text2 = "Drivers should treat impacted intersections as an all-way stop, except when directed by NYPD";
//  writeFlashing(text2, 0xFF00);
//  String text3 = "Drive with caution and always yield to pedestrians and cyclists";
//    writeFlashing(text3, 0xFF00);
	/*if ((loops % 100) == 0) {
		run_weather();
	} else {
		run_text();
	}*/

	//  render();
	//  delay(1000);
	//  run_incrementor();
	//run_disco();
	//  if ((loops % 5) == 0 || logo_loops != 10) {
	////    run_text();
	//    run_weather();
	//  } else {
	//    run_incrementor();
	////    run_disco();
	//  }
}

/*void loop() {
	loops++;

	//leds[128] = CRGB(0x67, 0xff, 0x81);
	leds[loops] = CRGB(0x67, 0xff, 0x81);

	if (loops > 255) {
	loops = 0;
	} else if (loops % 2) {
	leds[0] = CRGB(0, 0, 0);
	leds[1] = CRGB(0, 0, 0);
	leds[2] = CRGB(0, 0, 0);
	leds[3] = CRGB(0x67, 0xff, 0x81);
	FastLED.show();
	} else {
	//GRB
	leds[0] = CRGB(255, 0, 0);
	leds[1] = CRGB(0, 255, 0);
	leds[2] = CRGB(0, 0, 255);
	FastLED.show();
	}
	delay(500);
	// put your main code here, to run repeatedly:
}*/
