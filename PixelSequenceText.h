#ifndef PIXSEQTEXT_H
#define PIXSEQTEXT_H
#include <FastLED.h>
#include "PixelSequences.h"

enum PSTextType {
    qGeneric,
    qNearbyAlert,
    qNYCNotify,
    qMessage,
    qAlert, // Important alert
    qAmber
};

PSTextType _type;

String _displayMessageCache;
long _displayMessageCacheColor;
String _nextDisplayMessageCache;
long _nextDisplayMessageCacheColor;

void qrDisplayMessage() {
    // Serial.println(":" + _displayMessageCache);
    if (_displayMessageCache.length()) {
        writeFlashing(_displayMessageCache, _displayMessageCacheColor);
        _displayMessageCache = _nextDisplayMessageCache;
        _displayMessageCacheColor = _nextDisplayMessageCacheColor;
    } else if (_nextDisplayMessageCache.length()) {
        _displayMessageCache = _nextDisplayMessageCache;
        _displayMessageCacheColor = _nextDisplayMessageCacheColor;
        qrDisplayMessage();
    }
    // Serial.println("ml");
        Serial.println(_displayMessageCache.length());
}

void qsDisplayMessage(String &displayMessage, long messageColor, PSTextType typ) {
    _displayMessageCache = displayMessage;
    _displayMessageCacheColor = messageColor;
    // Serial.println(":" + _displayMessageCache);

    if (typ == PSTextType::qNearbyAlert) {
	    FastLED.setBrightness(5);
        String s = "NEARBY!";
        writeFlashing(s, 0xFFFF00, 3000);
	    FastLED.setBrightness(1);
    } else if (typ == PSTextType::qNYCNotify) {
        String s = "NYC ALRT";
        writeFlashing(s, 0xFF0000, 3000);
    } else if (typ == PSTextType::qAmber) {
        String h = "AMBER ALERT";

        FastLED.setBrightness(20);

        writeFlashing(h, 0xFF00, 3000);

        FastLED.setBrightness(1);
    }

    qrDisplayMessage();
}


void qFlush() {
    _displayMessageCache = "";
    _displayMessageCacheColor = 0;
    _nextDisplayMessageCache = "";
    _nextDisplayMessageCacheColor = 0;
    _type = 0;
}

#endif