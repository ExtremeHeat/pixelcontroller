#include <string.h>

enum SerialInputType :char {
	sMessage,
	sInteger
};

enum SerialReadingState {
    Working,
    AwaitingTimesync,
    RecievingBufferedACK
};

// SerialInputType expectedType = sMessage;

SerialReadingState sState = Working;

void sSendACK() { // Tells the server to proceed sending us next message
    Serial.println("@");
}

void sSendNAK() {
    Serial.println("!");
}

void sSendMessage(String message, bool needAck) {

}

#define MESSAGE_TYPE_COMMAND '~'
#define MESSAGE_TYPE_ACK '@'
#define MESSAGE_TYPE_NAK '!'
#define MESSAGE_TYPE_BUFFERED '.'

// void sSetReadingMode(SerialInputType type) {
//     expectedType = type;
// }

void sSetStateExpectingTimesync() {
    sState = SerialReadingState::AwaitingTimesync;
    // Serial.println("State -> AwaitingTimesync");
}

void sSetStateAwaitingBufferedMessage() {
    sState = SerialReadingState::RecievingBufferedACK;
    // Serial.println("State -> RecievingBufferedACK");
}

void sReset() {
    sState = Working;
}

void sProcessBufferedMessage(String &message) {
    auto index = message[1];
    auto end = message[2];

    if (message[3] == '.') {
        String m = message.substring(4, message.length());
        qsDisplayMessage(m, 0xff00, 0);
    } else { // TODO: handle color data

    }

    if (index == end) {
        qFlush();
        sReset();
    }
}

void sProcessMessage(String &message) {
    auto incoming = message;
    if (incoming[0] == MESSAGE_TYPE_COMMAND) {
        if (incoming[1] == 'T') {
            sSetStateExpectingTimesync();
        } else if (incoming[1] == 'M') { // message
            // Serial.println("PM TM!");
            auto mt = incoming[2];
            PSTextType typ = PSTextType::qMessage;
            switch (mt) {
                case 'C': // Nearby Alert 
                    typ = PSTextType::qNearbyAlert;
                    break;
                case 'Y': // NYC Notify
                    typ = PSTextType::qMessage;
                    break;
                case 'A': // Generic Alert
                    typ = PSTextType::qAlert;
                    break;
                case 'M': // Generic Message
                    typ = PSTextType::qMessage;
                    break;
            }

            auto opts = incoming[3];
            
            if (opts == 'B') { // Buffered flashing
                sSetStateAwaitingBufferedMessage();
                _type = typ;
            // } else if (opts == 'b') { // Buffered scrolling

            // } else if (opts == 'F') { // Flashing

            } else { // Scroll
                auto t = incoming.substring(4, incoming.length());
                incoming = ""; // mem clear
                Serial.println("Dis");
                Serial.println(t);
                qsDisplayMessage(t, 0xFF000000, typ);
            }

            sSendACK();
        }
    } else if (incoming[0] == MESSAGE_TYPE_BUFFERED) {
        if (sState == RecievingBufferedACK) {
            sProcessBufferedMessage(incoming);
        } else {
            sSendNAK();
        }
    } else if (incoming[0] == MESSAGE_TYPE_ACK) {
        if (sState == RecievingBufferedACK) {
            qFlush();
            sReset();
        }
    } else {
        //          writeFlashing(incoming, 0xB2222200);
        //    auto len = incoming.length();
        //    write(len, 0xFF00);
    }
}

void sProcessTimestamp(long utime) {
    sReset();
}